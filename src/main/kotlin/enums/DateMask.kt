package enums

enum class DateMask(val source: String) {
    DD_MM_YYYY("dd.MM.yyyy"),
    DD_MM("dd.MM"),
    MM_SLASH_YY("MM/yy"),
    DD_MM_YYYY_COMMA_HH_MM("dd.MM.yyyy, HH:mm"),
    DD_MM_YYYY_HH_MM("dd.MM.yyyy HH:mm"),
    DD_MMMM_YYYY_HH_MM("dd MMMM yyyy HH:mm"),
    DD_MMMM_HH_MM("dd MMMM HH:mm"),
    HH_MM("HH:mm"),
    DD_MMMM_YYYY("dd MMMM yyyy"),
    YYYY_MM_DD_T_HH_MM_SS_SSS("yyyy-MM-dd'T'HH:mm:ss.SSS"),
    YYYY_MM_DD_HH_MM_SS_SSS("yyyy-MM-dd-HH-mm-ss-SSS"),
    YYYY_MM_DD("yyyy-MM-dd")
}