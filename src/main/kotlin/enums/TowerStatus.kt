package enums

enum class TowerStatus {
    BUILDING,
    BUILT,
    FELL
}