package commands

import enums.DateMask
import managers.ApiManager
import java.util.*

class ExampleCommand(
    private val manager: ApiManager
) : BaseCommand(
    "Пример запроса"
) {

    override fun execute(scanner: Scanner, onEnd: () -> Unit) {
        val builder = manager.createPost()

        println("Введите значение для параметра `path`")
        val path = scanner.nextLine()

        builder.setUrl("/example/$path")

        println("Введите значение параметра `header`")
        val header = scanner.nextLine()
        builder.addHeader("header", header)

        println("Введите значение параметра `query`")
        val query = scanner.nextLine()
        builder.addQuery("query", query)

        println("Введите значение параметра `field`")
        val field = scanner.nextLine()
        builder.addFiled("field", field)

        builder.setBody(
            DateMask.DD_MM
        )

        builder.send()
        builder.send(onEnd)
    }
}
