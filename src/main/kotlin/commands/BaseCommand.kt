package commands

import java.util.*

abstract class BaseCommand(
    val text: String
){

    abstract fun execute(scanner: Scanner, onEnd: () -> Unit)
}