package entities

import com.google.gson.annotations.SerializedName

data class Letter(
    @SerializedName("name") val name: String,
    @SerializedName("x") val x: String,
    @SerializedName("y") val y: String,
    @SerializedName("z") val z: String
)