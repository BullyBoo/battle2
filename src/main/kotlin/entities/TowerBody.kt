package entities

import com.google.gson.annotations.SerializedName

class TowerBody(
    @SerializedName("towerId") val towerId: String,
    @SerializedName("letters") val letters: List<Letter>
)