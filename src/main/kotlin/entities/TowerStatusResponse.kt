package entities

import com.google.gson.annotations.SerializedName
import enums.TowerStatus

data class TowerStatusResponse(
    @SerializedName("status") val status: TowerStatus,
    @SerializedName("errors") val errors: List<String>,
    @SerializedName("wordsCount") val wordsCount: Int,
    @SerializedName("height") val height: Int
)