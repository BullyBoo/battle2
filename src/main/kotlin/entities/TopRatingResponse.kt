package entities

import com.google.gson.annotations.SerializedName

data class TopRatingResponse(
    @SerializedName("leaderboard") val leaderboard: List<RatingTowerRowData>,
    @SerializedName("teams") val teams: List<RatingTeamRowData>,
    @SerializedName("towers") val towers: List<RatingTowerRowData>
)