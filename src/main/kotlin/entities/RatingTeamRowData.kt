package entities

import com.google.gson.annotations.SerializedName

data class RatingTeamRowData(
    @SerializedName("id") val id: Double,
    @SerializedName("name") val name: String,
    @SerializedName("sentTowers") val sentTowers: Double,
    @SerializedName("builtTowers") val builtTowers: Double,
    @SerializedName("topHeight") val topHeight: Int,
    @SerializedName("topWords") val topWords: Int,
    @SerializedName("topLetters") val topLetters: Int,
    @SerializedName("topHeightTower") val topHeightTower: RatingTowerRowData,
    @SerializedName("topWordsTower") val topWordsTower: RatingTowerRowData,
    @SerializedName("topLettersTower") val topLettersTower: RatingTowerRowData
)