package entities

data class WordData(
    val value: String,
    val definition: String
)