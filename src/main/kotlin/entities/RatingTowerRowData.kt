package entities

import com.google.gson.annotations.SerializedName

data class RatingTowerRowData(
    @SerializedName("id") val id: Double,
    @SerializedName("name") val name: String,
    @SerializedName("teamId") val teamId: Double,
    @SerializedName("teamName") val teamName: String,
    @SerializedName("height") val height: Int,
    @SerializedName("wordsCount") val wordsCount: Int,
    @SerializedName("lettersCount") val lettersCount: Int
)