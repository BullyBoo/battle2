package utils

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonElement

internal class Deserializer<O>(private val classO: Class<O>) {

    fun deserialize(context: JsonDeserializationContext, element: JsonElement?): O? {
        return context.deserialize(element, classO)
    }
}