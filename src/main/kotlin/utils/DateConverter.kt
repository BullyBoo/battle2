package utils

import enums.DateMask
import java.text.DateFormatSymbols
import java.text.SimpleDateFormat
import java.util.*

object DateConverter {

    fun parse(
        date: String,
        mask: DateMask = DateMask.YYYY_MM_DD_T_HH_MM_SS_SSS,
        formatSymbols: DateFormatSymbols? = null,
        timeZone: TimeZone = TimeZone.getDefault()
    ): Long {
        if (date.isEmpty()) {
            return 0L
        }

        val format = SimpleDateFormat(mask.source, Locale.getDefault())
        format.timeZone = timeZone

        formatSymbols?.let { format.dateFormatSymbols = it }

        return format.parse(date)!!.time
    }

    fun parse(
        time: Long,
        mask: DateMask = DateMask.YYYY_MM_DD_T_HH_MM_SS_SSS,
        timeZone: TimeZone = TimeZone.getDefault(),
        formatSymbols: DateFormatSymbols? = null
    ): String {
        val format = SimpleDateFormat(mask.source, Locale.getDefault())
        formatSymbols?.let { format.dateFormatSymbols = it }
        format.timeZone = timeZone
        return format.format(Date(time))
    }

    fun parse(
        calendar: Calendar,
        mask: DateMask = DateMask.YYYY_MM_DD_T_HH_MM_SS_SSS,
        timeZone: TimeZone = TimeZone.getDefault(),
        formatSymbols: DateFormatSymbols? = null
    ): String {
        return parse(calendar.timeInMillis, mask, timeZone, formatSymbols)
    }

    fun convert(
        year: Int,
        month: Int,
        day: Int
    ): Calendar {
        return Calendar.getInstance().apply {
            set(Calendar.HOUR_OF_DAY, 0)
            set(Calendar.MINUTE, 0)
            set(Calendar.SECOND, 0)

            set(Calendar.YEAR, year)
            set(Calendar.MONTH, month)
            set(Calendar.DAY_OF_MONTH, day)
        }
    }

    fun convert(
        date: String,
        mask: DateMask = DateMask.YYYY_MM_DD_T_HH_MM_SS_SSS
    ): Calendar {
        val format = SimpleDateFormat(mask.source, Locale.getDefault())
        return Calendar.getInstance().apply {
            time = format.parse(date) ?: Date(0L)
        }
    }
}