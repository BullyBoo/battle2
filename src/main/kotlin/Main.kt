import commands.ExampleCommand
import managers.ApiManager
import java.util.*

object Main {

    private const val BASE_URL = "https://dtower-api.datsteam.dev/"

    private val apiManager = ApiManager(
        BASE_URL,
        hashMapOf(
            "token" to "XEy2YWc+GGTnLu6S4W3qM3TxVN1hpFBvPqErwFOIWJnnfdMbjEOBW/flF2zyeCq1BUk="
        )
    )

    private val commands = listOf(
        ExampleCommand(apiManager)
    )

    private var isAwait = false

    @JvmStatic
    fun main(args: Array<String>) {
//        val scanner = Scanner(System.`in`)
//
//        startMainMenu(scanner)

//        Observable.interval(2, TimeUnit.SECONDS)
//            .subscribe {
//                if(!isAwait){
////                    TODO
//                }
//            }


        apiManager.getWords()
            .subscribe ({ words ->

                words.forEach {

                }

                println()
            }, {
                println()
            })

        apiManager.getTopTowers()
            .subscribe()

        @Suppress("ControlFlowWithEmptyBody")
        while (true) { }
    }

    private fun start(){
//        apiManager.updateTower()
//            .subscribe {
//                isAwait = true
//            }
    }

    private fun startMainMenu(scanner: Scanner) {
        commands.forEachIndexed { index, command ->
            println((index + 1).toString() + " - " + command.text)
        }

        try {
            val command = scanner.nextLine().toInt()
            commands[command - 1].execute(scanner) {
                startMainMenu(scanner)
            }
        } catch (e: Exception) {
            e.printStackTrace()
            startMainMenu(scanner)
        }
    }
}