package extensions

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonObject
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializationContext
import enums.DateMask
import utils.DateConverter
import utils.Deserializer
import java.util.*
import kotlin.reflect.KClass

// region start
// String
fun JsonObject.getString(key: String): String {
    val jsonElement = this[key]
    return if (jsonElement is JsonPrimitive) {
        jsonElement.getAsString()
    } else {
        ""
    }
}

fun JsonObject.getNullableString(key: String): String? {
    val jsonElement = this[key]
    return if (jsonElement is JsonPrimitive) {
        jsonElement.getAsString()
    } else {
        null
    }
}
// region end


// region start
// Integer
fun JsonObject.getInteger(key: String): Int {
    val jsonElement = this[key]
    return (jsonElement as? JsonPrimitive)?.asInt ?: 0
}

fun JsonObject.getIntegerFromString(key: String): Int {
    val jsonElement = this[key]
    if (jsonElement is JsonPrimitive) {
        val asString = jsonElement.getAsString()
        return if (asString.isEmpty()) {
            0
        } else {
            try {
                asString.toInt()
            } catch (e: NumberFormatException) {
                0
            }
        }
    }
    return 0
}
// region end


// region start
// Float
fun JsonObject.getFloat(key: String): Float {
    val jsonElement = this[key]
    return (jsonElement as? JsonPrimitive)?.asFloat ?: 0f
}

fun JsonObject.getFloatFromString(key: String): Float {
    val jsonElement = this[key]
    if (jsonElement is JsonPrimitive) {
        val asString = jsonElement.getAsString()
        return if (asString.isEmpty()) {
            0f
        } else {
            try {
                asString.replace(",", ".").toFloat()
            } catch (e: NumberFormatException) {
                0f
            }
        }
    }
    return 0f
}

fun JsonObject.getFloatOrNullFromString(key: String): Float? {
    if (!this.has(key)) {
        return null
    }

    val jsonElement = this[key]
    if (jsonElement is JsonPrimitive) {
        val asString = jsonElement.getAsString()
        return if (asString.isEmpty()) {
            null
        } else {
            try {
                asString.replace(",", ".").toFloat()
            } catch (e: NumberFormatException) {
                null
            }
        }
    }
    return null
}
// region end


// region start
// Double
fun JsonObject.getDouble(key: String): Double {
    val jsonElement = this[key]
    return (jsonElement as? JsonPrimitive)?.asDouble ?: 0.0
}

fun JsonObject.getNullableDouble(key: String): Double? {
    val jsonElement = this[key]
    return (jsonElement as? JsonPrimitive)?.asDouble
}

fun JsonObject.getDoubleFromString(key: String): Double {
    val jsonElement = this[key]
    if (jsonElement is JsonPrimitive) {
        val asString = jsonElement.getAsString()
        return if (asString.isEmpty()) {
            0.0
        } else {
            try {
                asString.replace(",", ".").toDouble()
            } catch (e: NumberFormatException) {
                0.0
            }
        }
    }
    return 0.0
}
// region end


// region start
// Boolean
fun JsonObject.getBoolean(key: String): Boolean {
    val jsonElement = this[key]
    return (jsonElement as? JsonPrimitive)?.asBoolean ?: false
}

fun JsonObject.getNullableBoolean(key: String): Boolean? {
    val jsonElement = this[key]
    return (jsonElement as? JsonPrimitive)?.asBoolean
}
// region end


// region start
// Calendar
fun JsonObject.getCalendar(
    key: String,
    mask: DateMask = DateMask.YYYY_MM_DD_T_HH_MM_SS_SSS
): Calendar {
    val date = getString(key)

    val millis = try {
        DateConverter.parse(
            date = date,
            mask = mask,
            timeZone = TimeZone.getTimeZone("UTC")
        )
    } catch (e: Exception) {
        0L
    }

    return Calendar.getInstance().apply {
        timeInMillis = millis
    }
}

fun JsonObject.getNullableCalendar(
    key: String,
    mask: DateMask = DateMask.YYYY_MM_DD_T_HH_MM_SS_SSS
): Calendar? {
    val date = getString(key)

    return if (date.isEmpty()) {
        return null
    } else {
        getCalendar(key, mask)
    }
}
// region end


// region start
// Currency
fun JsonObject.getCurrency(key: String): Currency {
    return try {
        Currency.getInstance(getString(key))
    } catch (e: Exception) {
        Currency.getInstance(Locale.getDefault())
    }
}
// region end


// region start
// Object / Any
fun <M : Any> JsonObject.getObject(
    context: JsonDeserializationContext,
    key: String,
    objectClass: KClass<M>
): M {
    return Deserializer(objectClass.java).deserialize(context, this[key])!!
}

fun <M : Any> JsonObject.toObject(
    context: JsonDeserializationContext,
    objectClass: KClass<M>
): M {
    return Deserializer(objectClass.java).deserialize(context, this)!!
}

fun <M : Any> JsonObject.getNullableObject(
    context: JsonDeserializationContext,
    key: String?,
    classM: KClass<M>
): M? {
    return Deserializer(classM.java).deserialize(context, this[key])
}
// region end


// region start
// Enum
fun <M : Enum<M>> JsonObject.getEnum(
    context: JsonDeserializationContext,
    key: String,
    enumClass: KClass<M>,
    defaultValue: M
): M {
    return try {
        getNullableObject(context, key, enumClass) ?: defaultValue
    } catch (e: Exception) {
        defaultValue
    }
}

fun <M : Enum<M>> JsonObject.getNullableEnum(
    context: JsonDeserializationContext,
    key: String,
    classM: KClass<M>
): M? {
    return getNullableObject(context, key, classM)
}
// region end


// region start
// List
fun <M : Any> JsonObject.getList(
    key: String,
    context: JsonDeserializationContext,
    classM: KClass<M>
): List<M> {
    return try {
        this[key].asJsonArray.map {
            Deserializer(classM.java).deserialize(context, it)!!
        }
    } catch (e: Exception) {
        listOf()
    }
}

fun <M : Any> JsonObject.getListOfEnums(
    context: JsonDeserializationContext,
    key: String,
    enumClass: KClass<M>,
    defaultValue: M
): List<M> {
    return try {
        this[key].asJsonArray.map {
            try {
                getNullableObject(context, key, enumClass) ?: defaultValue
            } catch (e: Exception) {
                defaultValue
            }
        }
    } catch (e: Exception) {
        listOf()
    }
}

fun JsonObject.getListString(
    key: String
): List<String> {
    return try {
        this[key].asJsonArray.map {
            it.asString
        }
    } catch (e: Exception) {
        listOf()
    }
}

fun <M : Any> JsonObject.getNullableList(
    key: String,
    context: JsonDeserializationContext,
    classM: KClass<M>
): List<M>? {
    return if (has(key)) {
        getList(key, context, classM)
    } else {
        null
    }
}

fun JsonObject.getNullableListOfStrings(
    key: String
): List<String>? {
    return if (has(key)) {
        try {
            this[key].asJsonArray.map {
                it.asString
            }
        } catch (e: Exception) {
            listOf()
        }
    } else {
        null
    }
}
// region end


// region start
// Add Property
fun JsonObject.addCalendar(
    key: String,
    calendar: Calendar,
    mask: DateMask = DateMask.YYYY_MM_DD_T_HH_MM_SS_SSS
) {
    val date = DateConverter.parse(
        calendar = calendar,
        mask = mask,
        timeZone = TimeZone.getTimeZone("UTC")
    )
    addProperty(key, date)
}

fun JsonObject.addEnum(
    context: JsonSerializationContext,
    key: String,
    mEnum: Any
) {
    addObject(context, key, mEnum)
}

fun JsonObject.addObject(
    context: JsonSerializationContext,
    key: String,
    mObject: Any
) {
    val element = context.serialize(mObject)

    add(key, element)
}