package network.interceptors

import com.google.gson.GsonBuilder
import com.google.gson.JsonParser
import okhttp3.FormBody
import okhttp3.Interceptor
import okhttp3.Response
import okio.Buffer
import java.net.URLDecoder

class LoggerInterceptor : Interceptor {

    //    private val gson = GsonBuilder().setPrettyPrinting().create()
    private val gson = GsonBuilder().create()
    private val parser = JsonParser()

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder().build()

        println("*****************************************************************************************************")
        println("Request")
        println("-> ${request.method.capitalize()} ${request.url}")

        if (request.headers.size != 0) {
            println()
            println("Headers:")
            request.headers.forEach {
                println(it.first + ": " + it.second)
            }
        }

        request.body?.let {
            if (it is FormBody) {
                println()
                println("Form Data:")
                for (i in 0 until it.size) {
                    println(it.encodedName(i) + " = " + URLDecoder.decode(it.encodedValue(i)))
                }
            } else {
                println()
                println("Request Body:")

                val buffer = Buffer()
                it.writeTo(buffer)
                val source = buffer.readUtf8()
                println(source)
            }
        }

        try {
            println()
            println("Response")

            val response = chain.proceed(request)

            println("<- ${response.code}")

            if (response.headers.size != 0) {
                println()
                println("Headers:")
                request.headers.forEach {
                    println(it.first + ": " + it.second)
                }
            }
            response.body?.string()?.let {
                println()
                println("Response Body:")
                println(it)
            }

            println("*****************************************************************************************************")

            return response
        } catch (e: Exception) {
            e.printStackTrace()
            throw e
        }
    }
}