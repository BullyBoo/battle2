package network

import entities.TopRatingResponse
import entities.TowerStatusResponse
import io.reactivex.Completable
import io.reactivex.Single
import okhttp3.RequestBody
import retrofit2.http.*

interface Api {

    @GET
    fun getRequest(
        @Url url: String,
        @HeaderMap headerMap: Map<String, String> = hashMapOf(),
        @QueryMap queryMap: Map<String, String> = hashMapOf()
    ): Completable

    @FormUrlEncoded
    @POST
    fun postRequest(
        @Url url: String,
        @HeaderMap headerMap: Map<String, String> = hashMapOf(),
        @QueryMap queryMap: Map<String, String> = hashMapOf(),
        @FieldMap fieldMap: Map<String, String> = hashMapOf()
    ): Completable

    @POST
    fun postRequest(
        @Url url: String,
        @HeaderMap headerMap: Map<String, String> = hashMapOf(),
        @QueryMap queryMap: Map<String, String> = hashMapOf(),
        @Body body: RequestBody
    ): Completable

    @GET("/towers/{id}")
    fun getTowerData(
        @Path("id") id: String
    ): Single<TowerStatusResponse>

    @GET("/towers/top")
    fun getTopTowers(): Single<TopRatingResponse>
}