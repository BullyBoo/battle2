package network.converters

import com.google.gson.*
import java.lang.reflect.Type

abstract class BaseConverter<Model> : JsonDeserializer<Model>, JsonSerializer<Model> {

    override fun deserialize(
        json: JsonElement,
        typeOfT: Type,
        context: JsonDeserializationContext
    ): Model {
        return deserialize(json, context)
    }

    override fun serialize(
        src: Model,
        typeOfSrc: Type?,
        context: JsonSerializationContext?
    ): JsonElement {
        return serialize(src)
    }

    abstract fun deserialize(
        json: JsonElement,
        context: JsonDeserializationContext
    ): Model

    abstract fun serialize(
        source: Model,
    ): JsonElement
}