package network.converters

import com.google.gson.JsonElement
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import java.lang.reflect.Type

abstract class BaseSerializer<Model> : JsonSerializer<Model> {

    override fun serialize(
        src: Model,
        typeOfSrc: Type?,
        context: JsonSerializationContext
    ): JsonElement {
        return serialize(src, context)
    }

    abstract fun serialize(
        source: Model,
        context: JsonSerializationContext
    ): JsonElement
}