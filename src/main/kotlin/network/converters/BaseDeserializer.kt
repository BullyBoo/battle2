package network.converters

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import java.lang.reflect.Type

abstract class BaseDeserializer<Model> : JsonDeserializer<Model> {

    override fun deserialize(
        json: JsonElement,
        typeOfT: Type,
        context: JsonDeserializationContext
    ): Model {
        return deserialize(json.asJsonObject, context)
    }

    abstract fun deserialize(
        json: JsonObject,
        context: JsonDeserializationContext
    ): Model
}