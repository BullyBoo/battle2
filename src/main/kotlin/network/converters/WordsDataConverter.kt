package network.converters

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonObject
import entities.WordData
import entities.WordsData

class WordsDataConverter: BaseDeserializer<WordsData>() {

    override fun deserialize(json: JsonObject, context: JsonDeserializationContext): WordsData {
        val list = arrayListOf<WordData>()

        json.keySet()
            .forEach {
                list.add(
                    WordData(
                        value = it,
                        definition = json.get(it).asJsonObject.get("definition").asString
                    )
                )
            }

        return WordsData(list)
    }
}