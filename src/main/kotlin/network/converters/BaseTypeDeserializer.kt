package network.converters

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type

abstract class BaseTypeDeserializer<Model> : JsonDeserializer<Model> {

    override fun deserialize(
        json: JsonElement,
        typeOfT: Type,
        context: JsonDeserializationContext
    ): Model {
        return deserialize(json, context)
    }

    abstract fun deserialize(
        json: JsonElement,
        context: JsonDeserializationContext
    ): Model
}