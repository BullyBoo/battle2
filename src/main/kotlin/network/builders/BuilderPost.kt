package network.builders

import com.google.gson.Gson
import io.reactivex.Completable
import io.reactivex.schedulers.Schedulers
import network.Api
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody

class BuilderPost(
    private val api: Api,
    private val gson: Gson,
    private val defaultHeaders: Map<String, String>
) {

    private var url: String = ""
    private val headers = hashMapOf<String, String>()
    private val queries = hashMapOf<String, String>()
    private val fields = hashMapOf<String, String>()
    private var body: Any? = null

    fun setUrl(url: String): BuilderPost {
        this.url = url
        return this
    }

    fun addHeader(name: String, value: String): BuilderPost {
        headers[name] = value
        return this
    }

    fun addQuery(name: String, value: String): BuilderPost {
        queries[name] = value
        return this
    }

    fun addQuery(name: String, value: Int): BuilderPost {
        queries[name] = value.toString()
        return this
    }

    fun addQuery(name: String, value: Float): BuilderPost {
        queries[name] = value.toString()
        return this
    }

    fun addQuery(name: String, value: Double): BuilderPost {
        queries[name] = value.toString()
        return this
    }

    fun addQuery(name: String, value: Long): BuilderPost {
        queries[name] = value.toString()
        return this
    }

    fun addFiled(name: String, value: String): BuilderPost {
        fields[name] = value
        return this
    }

    fun addFiled(name: String, value: Int): BuilderPost {
        fields[name] = value.toString()
        return this
    }

    fun addFiled(name: String, value: Float): BuilderPost {
        fields[name] = value.toString()
        return this
    }

    fun addFiled(name: String, value: Double): BuilderPost {
        fields[name] = value.toString()
        return this
    }

    fun addFiled(name: String, value: Long): BuilderPost {
        fields[name] = value.toString()
        return this
    }

    fun setBody(body: Any): BuilderPost {
        this.body = body
        return this
    }

    fun send(): Completable {
        val headersMap = defaultHeaders.plus(headers)

        val request = if (body == null) {
            api.postRequest(
                url = url,
                headerMap = headersMap,
                queryMap = queries,
                fieldMap = fields
            )
        } else {
            val requestBody = gson.toJson(body)
                .toRequestBody("application/json".toMediaType())

            api.postRequest(
                url = url,
                headerMap = headersMap,
                queryMap = queries,
                body = requestBody
            )
        }

        return request
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.newThread())
    }

    fun send(onEnd: () -> Unit) {
        send()
            .subscribe({
                onEnd.invoke()
            }, {
                onEnd.invoke()
            })
    }
}