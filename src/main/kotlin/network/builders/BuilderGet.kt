package network.builders

import io.reactivex.Completable
import io.reactivex.schedulers.Schedulers
import network.Api

class BuilderGet(
    private val api: Api,
    private val defaultHeaders: Map<String, String>
) {

    private var url: String = ""
    private val headers = hashMapOf<String, String>()
    private val queries = hashMapOf<String, String>()

    fun setUrl(url: String): BuilderGet {
        this.url = url
        return this
    }

    fun addHeader(name: String, value: String): BuilderGet {
        headers[name] = value
        return this
    }

    fun addQuery(name: String, value: String): BuilderGet {
        queries[name] = value
        return this
    }

    fun addQuery(name: String, value: Int): BuilderGet {
        queries[name] = value.toString()
        return this
    }

    fun addQuery(name: String, value: Float): BuilderGet {
        queries[name] = value.toString()
        return this
    }

    fun addQuery(name: String, value: Double): BuilderGet {
        queries[name] = value.toString()
        return this
    }

    fun addQuery(name: String, value: Long): BuilderGet {
        queries[name] = value.toString()
        return this
    }

    fun addFiled(name: String, value: String): BuilderGet {
        queries[name] = value
        return this
    }

    fun addFiled(name: String, value: Int): BuilderGet {
        queries[name] = value.toString()
        return this
    }

    fun addFiled(name: String, value: Float): BuilderGet {
        queries[name] = value.toString()
        return this
    }

    fun addFiled(name: String, value: Double): BuilderGet {
        queries[name] = value.toString()
        return this
    }

    fun addFiled(name: String, value: Long): BuilderGet {
        queries[name] = value.toString()
        return this
    }

    fun send(): Completable {
        val headersMap = defaultHeaders.plus(headers)

        val request = api.getRequest(
            url = url,
            headerMap = headersMap,
            queryMap = queries
        )

        return request
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.newThread())
    }

    fun send(onEnd: () -> Unit) {
        send()
            .subscribe({
                onEnd.invoke()
            }, {
                onEnd.invoke()
            })
    }
}