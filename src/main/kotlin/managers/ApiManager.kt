package managers

import com.google.gson.GsonBuilder
import entities.*
import io.reactivex.Completable
import io.reactivex.Single
import network.Api
import network.builders.BuilderGet
import network.builders.BuilderPost
import network.converters.WordsDataConverter
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.BufferedReader
import java.util.concurrent.TimeUnit

class ApiManager(
    baseUrl: String,
    private val defaultHeaders: Map<String, String> = hashMapOf()
){

    companion object {

        private const val DEFAULT_TIMEOUT_SECONDS = 30L
    }

    private val okHttpClient = OkHttpClient.Builder()
        .addInterceptor(
            HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            }
        )
        .retryOnConnectionFailure(true)
        .callTimeout(DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
        .connectTimeout(DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
        .readTimeout(DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
        .writeTimeout(DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
        .build()

    private val gson = GsonBuilder()

        .registerTypeAdapter(WordsData::class.java, WordsDataConverter())

        .create()

    private val retrofit = Retrofit.Builder()
        .client(okHttpClient)
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()

    private val api = retrofit.create(Api::class.java)

    fun createGet(): BuilderGet {
        return BuilderGet(api, defaultHeaders)
    }

    fun createPost(): BuilderPost {
        return BuilderPost(api, gson, defaultHeaders)
    }

    fun getWords(): Single<List<WordData>> {
        return Single.fromCallable {
            val stream = javaClass.classLoader.getResourceAsStream("map.json")!!
            val reader = BufferedReader(stream.reader())
            val json = reader.readText()
            val data = gson.fromJson(json, WordsData::class.java)

            data.words
        }
    }

    fun updateTower(id: String, letter: List<Letter>): Completable {
        return createPost()
            .setUrl("tower")
            .setBody(
                TowerBody(
                    towerId = id,
                    letters = letter
                )
            )
            .send()
    }

    fun getTowerStatus(id: String): Single<TowerStatusResponse>{
        return api.getTowerData(id)
    }

    fun getTopTowers(): Single<TopRatingResponse>{
        return api.getTopTowers()
    }
}